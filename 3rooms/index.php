<!DOCTYPE html>
<html>
<head>
	<title>Viva загородный поселок</title>
	<meta name="Description" content="Сравнение коттеджных поселков Уфы"/>
	<meta charset="utf-8">
    <meta name="theme-color" content="#2E5FA3">
    <link rel="shortcut icon" href="/favicon.ico"/>
    <!-- <link rel="manifest" href="/manifest.json"> -->
    <meta name="viewport" content="width=480, minimum-scale=0.75, maximum-scale=1.25">
<!-- Проверить:для социалок
  	<meta property="og:url" content="http://viva-ufa.ru" /> 
  	<meta property="og:image" content="http://viva-ufa.ru/imgs/ogimg.png" />
  	<meta property="og:title" content="Viva-Уфа загородный поселок" />
  	<meta property="og:description" content="" />
  	<meta property="og:type" content="website" />
  	<meta property="fb:app_id" content="257953674358265" />
  	<meta name="format-detection" content="telephone=no" />
  	<link rel="canonical" href="http://viva-ufa.ru/">
  	<link rel="icon" href="http://viva-ufa.ru/favicon.ico" type="image/x-icon" />
  	<link rel="shortcut icon" href="http://viva-ufa.ru/favicon.ico" type="image/x-icon" />
 -->

<!-- Стили -->
	<link rel="stylesheet" href="/endtable_styles.css"/>

<!-- Скрипты -->
    <script type="text/javascript" src="/scripts/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="/scripts/main.js"></script>
</head>

<body>
        <div class="header">
            <a class="headerlogo">
                <svg height="40"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#viva_logo"></use></svg>
            </a>
            <div class="contacts">
                <!-- <a href="tel:+79991234321" class="headertell tellmob">8 (347)<span>266-18-50</span></a> -->
                <a class="headertell tellpc">8 (347)<span>266-18-50</span></a>
                <p>Уфа, Менделеева 217а, эт. 2, оф. 57</p>
            </div>
        </div>

        <!--  TABLE  -->
        <div class="endtable_base">
            <iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTuDE49qkVX5jgzS3Oa9UPm2Ik2l70pQOOyS8RRzWGSbvvT-62kc3athXaQfwKAES8YHsg8oW6XEjZ7/pubhtml?gid=0&amp;range=a1:l22&amp;single=true&amp;widget=false&amp;headers=false&amp;chrome=false"></iframe>
        </div>

        <!--  FOOTER  -->
        <footer>
            <div class="foot_container">
                <p>viva©2018</p>
                <p>разработано <a target="_blank" href="http://vk.me/mahaone">maha1dsgn</a></p>
            </div>
        </footer>
</body>
</html>