<!DOCTYPE html>
<html>
<head>
	<title>Viva загородный поселок</title>
	<meta name="Description" content="Загородный поселок Вива, коттеджи и дома в Нагаевском парке"/>
	<meta charset="utf-8">
    <meta name="theme-color" content="#2E5FA3">
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="icon" href="http://viva-ufa.ru/favicon.ico" type="image/x-icon"/>
    <!-- <link rel="manifest" href="/manifest.json"> -->
    <meta name="viewport" content="width=720, minimum-scale=0.33, maximum-scale=1.25">
  	<meta property="og:url" content="http://viva-ufa.ru" /> 
  	<meta property="og:image" content="http://viva-ufa.ru/imgs/ogimg.png" />
  	<meta property="og:title" content="Viva загородный поселок" />
  	<meta property="og:description" content="Загородный поселок Вива, коттеджи и дома в Нагаевском парке" />
  	<meta property="og:type" content="website"/>
    <link rel="canonical" href="http://viva-ufa.ru/">
    <meta name="format-detection" content="telephone=no" />

<!-- Стили -->
	<link rel="stylesheet" href="/styles-common.css"/>
    <link rel="stylesheet" href="/styles-small.css" media="screen and (max-width:1390px)"/>
    <link rel="stylesheet" href="/styles-mobile.css" media="screen and (max-width:800px)"/>

<!-- Скрипты -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter48981599 = new Ya.Metrika({
                        id:48981599,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/48981599" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

    <script type="text/javascript" src="/scripts/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/scripts/lazyload.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="/scripts/main.js"></script>
</head>

<body>
    <div id="container_content">

        <header>
        <!-- HEADER -->
            <div class="header">
                <a class="headerlogo">
                    <svg height="40"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#viva_logo"></use></svg>
                </a>
                <div class="contacts">
                    <a href="tel:+73472661850" class="headertell tellmob">8 (347)<span>266-18-50</span></a>
                    <a class="headertell tellpc">8 (347)<span>266-18-50</span></a>
                    <p>Уфа, Менделеева 217а, эт. 2, оф. 57</p>
                </div>
            </div>
            <h1>Коттеджный поселок в&nbsp;пригороде&nbsp;Уфы</h1>
            <div class="descript">
                <p>Хвойный лес, озера и&nbsp;все современные условия&nbsp;для&nbsp;жизни</p>
            </div>
            <div class="submaintxt">
                <ul>
                    <li>
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#check_mini"></use></svg>
                        5&nbsp;типов коттеджей с&nbsp;участком
                    </li>
                    <li>
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#check_mini"></use></svg>
                        Все&nbsp;коммуникации уже&nbsp;проведены
                    </li>
                    <li>
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#check_mini"></use></svg>
                        Хорошие дороги до&nbsp;поселка и&nbsp;готовый асфальт к&nbsp;каждому&nbsp;дому
                    </li>
                    <img src="/imgs/home_visual.png" alt="двухэтажный дом поселок вива">
                </ul>
            </div>
        </header>

        <!--  CTA BLOCK  -->
        <div class="cta_background">
            <section class="ctablock">
                <p class="cta_title">Мы&nbsp;честно сравнили 10&nbsp;коттеджных&nbsp;поселков&nbsp;Уфы.<br>Получите обзор, ответив на&nbsp;4&nbsp;вопроса:</p>
                <div class="cta_right">
                    <a type="button" class="gotoquiz" onClick="showquiz();yaCounter48981599.reachGoal('quiz_start');return true;">
                        <svg class="pdf"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#pdficon"></use></svg>
                        <p>Получить сравнительный обзор</p>
                        <svg class="ctaarrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                    </a>
                    <p class="gotoquiz_subtxt">* Мы&nbsp;уверены в&nbsp;качестве наших домов и&nbsp;поэтому предлагаем&nbsp;Вам сравнить дома&nbsp;VIVA с&nbsp;другими предложениями.</p>
                </div>
            </section>
        </div>

        <!--  SLIDER 2  -->
        <section class="slider">
            <h2>Знакомьтесь, коттеджный поселок VIVA</h2>
            <div class="container_gallery">
            	<button disabled class="slider-prev slider-arrow">
					<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
	            </button>
	            <div class="slider_prev_container">
                    <img class="lazyload" src="/imgs/gallery/home01.jpg" data-src="/imgs/gallery/home01.jpg"/>
                    <img class="lazyload" src="/imgs/gallery/home02.jpg" data-src="/imgs/gallery/home02.jpg"/>
                    <img class="lazyload" src="/imgs/gallery/home03.jpg" data-src="/imgs/gallery/home03.jpg"/>
                    <img class="lazyload" src="/imgs/gallery/home04.jpg" data-src="/imgs/gallery/home04.jpg"/>
                    <img class="lazyload" src="/imgs/gallery/home05.jpg" data-src="/imgs/gallery/home05.jpg"/>
                    <img class="lazyload" src="/imgs/gallery/home06.jpg" data-src="/imgs/gallery/home06.jpg"/>
	            </div>
            	<button class="slider-next slider-arrow">
					<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
	            </button>
            </div>
        </section>



        <!--  FOOTER  -->
        <footer>
            <div class="foot_container">
                <p>viva©2018</p>
                <a class="gotopolicy" target="_blank" href="/policy">пользовательское соглашение</a>
                <p>разработано <a target="_blank" href="http://vk.me/mahaone">maha1dsgn</a></p>
            </div>
        </footer>

    </div>

<!--  QUIZ  -->
    <div id="container_quiz" class="pop_container">
        <form class="quiz_base" onsubmit="yaCounter48981599.reachGoal('quiz_submit');return true;" onclick="">
        <!-- Вопрос 1  -->
            <div class="quiz_pages page_01 page_active">
                <div class="progressbar">
                    <a class="cur">
                        <p>Количество комнат</p>
                    </a>
                    <a >
                        <p>Площадь участка</p>
                    </a>
                    <a>
                        <p>Стадия реализации</p>
                    </a>
                    <a>
                        <p>Вариант отделки</p>
                    </a>
                    <div class="popclose" onClick="hidequiz()">
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use></svg>
                    </div>
                </div>
                <div class="option_container">
                    <h3>Количество спальных комнат</h3>
                    <div class="quest_body">
                        <input type="radio" name="ans01" id="ans11" value="2 спальных комнаты">
                        <label for="ans11" onclick="">
                            <img src="/imgs/ans/ans11.png" alt="2 спальных комнаты">
                            <p class="ans_title">2 спальных комнаты</p>
                            <ul>
                                <li>Экономичность содержания</li>
                                <li>Легко поддерживать чистоту</li>
                                <li>Доступно*</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans01" id="ans12" value="3 спальных комнаты">
                        <label for="ans12" onclick="">
                            <img src="/imgs/ans/ans12.png" alt="2 спальных комнаты">
                            <p class="ans_title">3 спальных комнаты</p>
                            <ul>
                                <li>Подходит для небольшой семьи</li>
                                <li>Дополнительное место для гостей</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans01" id="ans13" value="4 спальных комнаты">
                        <label for="ans13" onclick="">
                            <img src="/imgs/ans/ans13.png" alt="2 спальных комнаты">
                            <p class="ans_title">4 спальных комнаты</p>
                            <ul>
                                <li>Для большой семьи</li>
                                <li>Можно принимать много гостей</li>
                                <li>Есть второй этаж</li>
                            </ul>
                        </label>
                    </div>
                </div>
                <div class="quest_buts">
                    <p class="steptip">Выберите один из&nbsp;вариантов</p>
                    <button disabled type="button" class="nextstep">
                        Дальше
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                    </button>
                </div>
            </div>

        <!-- Вопрос 2  -->
            <div class="quiz_pages page_02">
                <div class="progressbar">
                    <a class="pass">
                        <p>Количество комнат</p>
                    </a>
                    <a class="cur">
                        <p>Площадь участка</p>
                    </a>
                    <a>
                        <p>Стадия реализации</p>
                    </a>
                    <a>
                        <p>Вариант отделки</p>
                    </a>
                    <div class="popclose quiz_close" onClick="hidequiz()">
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use></svg>
                    </div>
                </div>
                <div class="option_container">
                    <h3>Площадь земельного участка</h3>
                    <div class="quest_body" id="qubody_01">
                        <input type="radio" name="ans02" id="ans21" value="до 7 соток">
                        <label for="ans21">
                            <img src="/imgs/ans/ans21.png" alt="до 7 соток">
                            <p class="ans_title">до 7 соток</p>
                            <ul>
                                <li>Легкий уход и обслуживание участка</li>
                                <li>Экономия на обустройстве ограждения</li>
                                <li>Простота планирования</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans02" id="ans22" value="до 10 соток">
                        <label for="ans22">
                            <img src="/imgs/ans/ans22.png" alt="до 10 соток">
                            <p class="ans_title">до 10 соток</p>
                            <ul>
                                <li>Возможность размещения необходимых хозяйственных и приусадебных построек</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans02" id="ans23" value="больше 10 соток">
                        <label for="ans23">
                            <img src="/imgs/ans/ans23.png" alt="больше 10 соток">
                            <p class="ans_title">больше 10 соток</p>
                            <ul>
                                <li>Простор для ландшафтного дизайна</li>
                                <li>Надежное вложение денег (цена дома на большом участке высокая)</li>
                            </ul>
                        </label>
                    </div>
                </div>
                <div class="quest_buts">
                    <p class="steptip">Выберите один из&nbsp;вариантов</p>
                    <button disabled type="button" class="nextstep" onClick="yaCounter48981599.reachGoal('quiz_2ans');return true;">
                        Дальше
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                    </button>
                </div>
            </div>

        <!-- Вопрос 3  -->
            <div class="quiz_pages page_03">
                <div class="progressbar">
                    <a class="pass">
                        <p>Количество комнат</p>
                    </a>
                    <a class="pass">
                        <p>Площадь участка</p>
                    </a>
                    <a class="cur">
                        <p>Стадия реализации</p>
                    </a>
                    <a>
                        <p>Вариант отделки</p>
                    </a>
                    <div class="popclose quiz_close" onClick="hidequiz()">
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use></svg>
                    </div>
                </div>
                <div class="option_container">
                    <h3>Стадия реализации объекта</h3>
                    <div class="quest_body">
                        <input type="radio" name="ans03" id="ans31" value="Готовый дом">
                        <label class="wide" for="ans31">
                            <img src="/imgs/ans/ans31.png" alt="Готовый дом">
                            <p class="ans_title">Готовый дом</p>
                            <ul>
                                <li>Заселение в кратчайшие сроки</li>
                                <li>Возможность приобретения в ипотеку</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans03" id="ans32" value="На стадии строительства">
                        <label class="wide" for="ans32">
                            <img src="/imgs/ans/ans32.png" alt="На стадии строительства">
                            <p class="ans_title">На стадии строительства</p>
                            <ul>
                                <li>Возможность контролировать ход строительства</li>
                                <li>Цена ниже</li>
                                <li>Возможность внесения изменений в проект</li>
                            </ul>
                        </label>
                    </div>
                </div>
                <div class="quest_buts">
                    <p class="steptip">Выберите один из&nbsp;вариантов</p>
                    <button disabled type="button" class="nextstep">
                        Дальше
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                    </button>
                </div>
            </div>

        <!-- Вопрос 4  -->
            <div class="quiz_pages page_04">
                <div class="progressbar complied">
                    <a class="pass">
                        <p>Количество комнат</p>
                    </a>
                    <a class="pass">
                        <p>Площадь участка</p>
                    </a>
                    <a class="pass">
                        <p>Стадия реализации</p>
                    </a>
                    <a class="cur">
                        <p>Вариант отделки</p>
                    </a>
                    <div class="popclose quiz_close" onClick="hidequiz()">
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use></svg>
                    </div>
                </div>
                <div class="option_container">
                    <h3>Вариант отделки</h3>
                    <div class="quest_body">
                        <input type="radio" name="ans04" id="ans41" value="Черновая">
                        <label for="ans41">
                            <img src="/imgs/ans/ans41.jpg" alt="Черновая">
                            <p class="ans_title">Черновая</p>
                            <ul>
                                <li>Даст вам возможность сделать именно то что вы захотите</li>
                                <li>Возможность наблюдать и контролировать отделочные работы</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans04" id="ans42" value="Предчистовая">
                        <label for="ans42">
                            <img src="/imgs/ans/ans42.jpg" alt="Предчистовая">
                            <p class="ans_title">Предчистовая</p>
                            <ul>
                                <li>Позволить сделать быстрый и недорогой ремонт своими силами</li>
                            </ul>
                        </label>
                        <input type="radio" name="ans04" id="ans43" value="Чистовая">
                        <label for="ans43">
                            <img src="/imgs/ans/ans43.jpg" alt="Чистовая">
                            <p class="ans_title">Чистовая</p>
                            <ul>
                                <li>Вы получите всё и сразу, без лишних хлопот</li>
                            </ul>
                        </label>
                    </div>
                </div>
                <div class="quest_buts">
                    <p class="steptip">Выберите один из&nbsp;вариантов</p>
                    <button disabled type="button" class="nextstep" onClick="yaCounter48981599.reachGoal('quiz_finish');return true;">
                        Завершить
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                    </button>
                </div>
            </div>

        <!-- Форма сбора данных  -->
            <div class="quiz_pages page_form">
                <div class="progressbar complied">
                    <a class="pass">
                        <p>Количество комнат</p>
                    </a>
                    <a class="pass">
                        <p>Площадь участка</p>
                    </a>
                    <a class="pass">
                        <p>Стадия реализации</p>
                    </a>
                    <a class="pass">
                        <p>Вариант отделки</p>
                    </a>
                    <div class="popclose quiz_close" onClick="hidequiz()">
                        <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use></svg>
                    </div>
                </div>
                <div class="option_container">
                    <h3>Спасибо за ответы!</h3>
                    <!-- <p class="subh3">Отправьте заявку и получите сравнительный обзор, оставив контакты:</p> -->
                    <p class="subh3">Получите сравнительный обзор, оставив контакты:</p>
                    <div class="quest_body">
                        <input type="hidden" name="utm_source" value="<?php echo $_GET['utm_source']?>" />
                        <div class="inpcont">
                            <input type="text" name="name">
                            <span>Ваше имя:</span>
                        </div>
                        <div class="inpcont">
                            <input required type="tel" id="usrtell" name="tell" placeholder="+7(800) 123-12-12"/>
                            <span>Телефон:</span>
                        </div>
                        <button type="submit" class="sendform">
                            Получить обзор
                            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                        </button>
                    </div>
                    <div class="tablesample">
                        <a target="_blank" href="/2rooms" class="tablelink" onClick="yaCounter48981599.reachGoal('endtable');return true;">
                            <svg class="pdf"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#pdficon"></use></svg>
                            <p>Открыть таблицу в&nbsp;новом&nbsp;окне</p>
                            <svg class="ctaarrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
                        </a>
                    </div>
                </div>
                <p class="privacy">Нажимая на&nbsp;кнопку «Получить&nbsp;обзор» вы&nbsp;даете согласие на&nbsp;обработку персональных данных и&nbsp;соглашаетесь с&nbsp;<a class="gotopolicy" target="_blank" href="/policy">политикой&nbsp;конфеденциальности</a></p>
            </div>
        </form>
    </div>

    <!--  SLIDER FULLSIZE  -->
    <div id="container_bigslider" class="pop_container">
        <div class="bigslider_base">
            <div class="popclose" onClick="hideslide()">
                <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use></svg>
            </div>
			<button disabled class="slider-fs-prev slider-arrow">
				<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
		    </button>
	        <div class="slider_fs_container">
                <img class="lazyload" src="/imgs/gallery/home01.jpg" data-src="/imgs/gallery/home01.jpg"/>
                <img class="lazyload" src="/imgs/gallery/home02.jpg" data-src="/imgs/gallery/home02.jpg"/>
                <img class="lazyload" src="/imgs/gallery/home03.jpg" data-src="/imgs/gallery/home03.jpg"/>
                <img class="lazyload" src="/imgs/gallery/home04.jpg" data-src="/imgs/gallery/home04.jpg"/>
                <img class="lazyload" src="/imgs/gallery/home05.jpg" data-src="/imgs/gallery/home05.jpg"/>
                <img class="lazyload" src="/imgs/gallery/home06.jpg" data-src="/imgs/gallery/home06.jpg"/>
	        </div>
	    	<button class="slider-fs-next slider-arrow">
				<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#nextstep_arrow"></use></svg>
	        </button>
        </div>
    </div>

</body>
</html>