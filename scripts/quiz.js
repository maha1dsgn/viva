$(document).ready(function(){

	var ajax = new XMLHttpRequest();
	ajax.open("GET", "/imgs/allimgs.svg", true);
	ajax.send();
	ajax.onload = function(e) {
		var div = document.createElement("div");
		div.id = "svgcontainer";
		div.style = "display:none";
		div.innerHTML = ajax.responseText;
		document.body.insertBefore(div, document.body.childNodes[0]);
	}

	var radiobox = $('.page_active').find('input[type="radio"]'),
    nextbut = $('.page_active').find('.nextstep'),
    pickclick = 'click';
    // var block_width = $('.header').width();
    $quizpages = $('.quiz_pages.page_active');
	nextbut.attr('disabled', !radiobox.is(':checked'));
	$('.steptip').css('display', 'inline', !radiobox.is(':checked'));
	// if (block_width >= 800) { pickclick = 'click' };
	// if (block_width < 800) { pickclick = 'touchend' };

	radiobox.change(function(){
		nextbut.attr('disabled', !radiobox.is(':checked'));
		$('.steptip').css('display', 'none', !radiobox.not(':checked'));
		if ($('input[name="ans01"]').filter(':checked').attr('id') == 'ans11'){
			$('.tablelink').attr('href', '/2rooms');
		};
		if ($('input[name="ans01"]').filter(':checked').attr('id') == 'ans12'){
			$('.tablelink').attr('href', '/3rooms');
		};
		if ($('input[name="ans01"]').filter(':checked').attr('id') == 'ans13'){
			$('.tablelink').attr('href', '/4rooms');
		};
	});

	$('.nextstep').on(pickclick, function(e) {
	    $quizpages.removeClass('page_active');
		$quizpages.next('.quiz_pages').addClass('page_active');
		$quizpages = $('.quiz_pages.page_active');
		radiobox = $('.page_active').find('input[type="radio"]');
		nextbut = $('.page_active').find('.nextstep');
		nextbut.attr('disabled', !radiobox.is(':checked'));
		$('.steptip').css('display', 'inline', !radiobox.is(':checked'));

		radiobox.change(function(){
			nextbut.attr('disabled', !radiobox.is(':checked'));
			$('.steptip').css('display', 'none', !radiobox.not(':checked'));
		});
	});

	$('.gotoquiz').on(pickclick, function(e) {
		$('#container_content').addClass('blured');
		$('#container_quiz').css('visibility', 'visible');
	});

	$('.gotopolicy').on(pickclick, function(e) {
		$('#container_content').addClass('blured');
		$('#container_policy').css('visibility', 'visible');
	});

	$('.container_gallery').find('img').on(pickclick, function(e) {
        $('#container_content').addClass('blured');
        $('#container_bigslider').css('visibility', 'visible');
	});

	$('.endclose').on(pickclick, function(e) {
		$('.quiz_base')[0].reset();
		$('.quiz_pages').removeClass('page_active');
		$('.page_01').addClass('page_active');
		$('#container_content').removeClass('blured');
		$(this).closest('.pop_container').css('visibility', 'hidden');
	});

	$('.popclose').on(pickclick, function(e) {
		$('.page_active').find('input[type="radio"]').prop('checked', false);
		$(this).closest('.pop_container').css('visibility', 'hidden');
		$('#container_content').removeClass('blured');
	});


	$('.gallery_big').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		asNavFor: '.container_gallery'
	});

	$('.container_gallery').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 2,
		dots: true,
		lazyLoad: 'ondemand',
		autoplay: true,
  		autoplaySpeed: 3500,
  		asNavFor: '.gallery_big',
  		focusOnSelect: true,
  		centerMode: true,
  		draggable: false,
  		centerPadding: '0',
		responsive: [
			{
				breakpoint: 800,
				settings: {
					speed: 200,
					swipe: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					asNavFor: false,
					focusOnSelect: false,
					centerMode: false,
					draggable: true
					}
			}
		]
	});

 $(".quiz_base").submit (function () {
   var name = $("input[name='name']").val();
   var tell = $("input[name='tell']").val();
   var ans01 = $("input[name='ans01']:checked").val();
   var ans02 = $("input[name='ans02']:checked").val();
   var ans03 = $("input[name='ans03']:checked").val();
   var ans04 = $("input[name='ans04']:checked").val();
   var utm = $("input[name='utm_source']").val();

    $.ajax({
      type: "POST",
      url: "/scripts/sendmail.php",
      data: {name: name, tell: tell, ans01: ans01, ans02: ans02, ans03: ans03, ans04: ans04, utm},
      success: function(data) {

        if(data) {
          $('.page_form').find('button').attr('disabled', true);
          $('.tablelink').css('display', 'block');
          $('.tablesample').find('img').css('display', 'none');
          $('.page_form').find('.popclose').removeClass('quiz_close');
          $('.page_form').find('.popclose').addClass('endclose');
        }
        else {
          alert("Не удалось отправить данные, попробуйте еще раз");
        }

      }

    });
    return false;
 });

});