$(document).ready(function(){
    $(".quiz_base").submit (function () {
        var name = $("input[name='name']").val();
        var tell = $("input[name='tell']").val();
        var ans01 = $("input[name='ans01']:checked").val();
        var ans02 = $("input[name='ans02']:checked").val();
        var ans03 = $("input[name='ans03']:checked").val();
        var ans04 = $("input[name='ans04']:checked").val();

        $.ajax({
            url: 'https://a.wunderlist.com/api/v1/tasks',
            method: 'POST',
            contentType: 'application/json',
            processData: false,
            headers: {
                'X-Access-Token': '032aad51de0f958bd9e185e98c1a56a5f326e9296e8989f5fa1a4c80d186',
                'X-Client-ID': '97c2fe156c266d4e2e51'
            },
            data: {
                "list_id": 298881342,
                "title": tell.toString(),
                "completed": false,
                "starred": false
            }
        }).success(function(data){
            swal("Task created!", "Your task was successfully created!", "success");
        }).error(handleError);
    });
});