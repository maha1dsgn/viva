function showquiz(){
	container_content.className = "blured";
	container_quiz.style.display = "flex";
}

function hidequiz(){
	container_content.className = "";
	container_quiz.style.display = "none";
}

function showpolicy(){
	container_content.className = "blured";
	container_policy.style.display = "flex";
}

function hidepolicy(){
	container_content.className = "";
	container_policy.style.display = "none";
}

function hideslide(){
	container_content.className = "";
	container_bigslider.style.display = "none";
}

$(document).ready(function(){

// SVG
	var ajax = new XMLHttpRequest();
	ajax.open("GET", "/imgs/allimgs.svg", true);
	ajax.send();
	ajax.onload = function(e) {
		var div = document.createElement("div");
		div.id = "svgcontainer";
		div.style = "display:none";
		div.innerHTML = ajax.responseText;
		document.body.insertBefore(div, document.body.childNodes[0]);
	};

// LAZY LOAD
$("img.lazy").lazyload({
		effect : "fadeIn",
		threshold : 0
	});

// QUIZ
	var
	radiobox = $('.page_active').find('input[type="radio"]'),
    nextbut = $('.page_active').find('.nextstep');
    $quizpages = $('.quiz_pages.page_active');
	nextbut.attr('disabled', !radiobox.is(':checked'));
	$('.steptip').css('display', 'inline', !radiobox.is(':checked'));

	radiobox.change(function(){
		nextbut.attr('disabled', !radiobox.is(':checked'));
		$('.steptip').css('display', 'none', !radiobox.not(':checked'));
		if ($('input[name="ans01"]').filter(':checked').attr('id') == 'ans11'){
			$('.tablelink').attr('href', '/2rooms');
		};
		if ($('input[name="ans01"]').filter(':checked').attr('id') == 'ans12'){
			$('.tablelink').attr('href', '/3rooms');
		};
		if ($('input[name="ans01"]').filter(':checked').attr('id') == 'ans13'){
			$('.tablelink').attr('href', '/4rooms');
		};
	});

	$('.nextstep').click(function() {
	    $quizpages.removeClass('page_active');
		$quizpages.next('.quiz_pages').addClass('page_active');
		$quizpages = $('.quiz_pages.page_active');
		radiobox = $('.page_active').find('input[type="radio"]');
		nextbut = $('.page_active').find('.nextstep');
		nextbut.attr('disabled', !radiobox.is(':checked'));
		$('.steptip').css('display', 'inline', !radiobox.is(':checked'));

		radiobox.change(function(){
			nextbut.attr('disabled', !radiobox.is(':checked'));
			$('.steptip').css('display', 'none', !radiobox.not(':checked'));
		});
	});

// TELL MASK
	$("#usrtell").mask("+7(999) 999-99-99");

// SLIDER
	var scrollWidth = 0;
	var scrollFsWidth = 0;
	var currentScrollOffset = 0;
	var currentFsScrollOffset = 0;
	var slwidth = $('.slider_prev_container').width();
	var slfswidth = $('.slider_fs_container').width();

	$( '.slider-next' ).click( function () { 
		scrollWidth = $(this).prev('.slider_prev_container').find('img:first').width();
		$('.slider-prev').attr('disabled', false);
		currentScrollOffset += scrollWidth;
		if($('.slider_prev_container')[0].scrollWidth - currentScrollOffset <=  slwidth) {
			$('.slider-next').attr('disabled', true);
		}
		scrollTo(currentScrollOffset, 0);
	});

	$( '.slider-prev' ).click( function () {
		scrollWidth = $(this).next('.slider_prev_container').find('img:first').width();
		currentScrollOffset -= scrollWidth;
		$('.slider-next').attr('disabled', false);
		if(currentScrollOffset <= 0) {
			currentScrollOffset = 0;
			$('.slider-prev').attr('disabled', true);
		}
		scrollTo(currentScrollOffset, 0);
	});

	$( '.slider-fs-next' ).click( function () { 
		scrollFsWidth = $(this).prev('.slider_fs_container').find('img:first').width();
		$('.slider-fs-prev').attr('disabled', false);
		currentFsScrollOffset += scrollFsWidth;
		if($('.slider_fs_container')[0].scrollFsWidth - currentFsScrollOffset <  slfswidth) {
			$('.slider-fs-next').attr('disabled', true);
		}
		scrollFsceenTo(currentFsScrollOffset, 0);
	});

	$( '.slider-fs-prev' ).click( function () {
		scrollFsWidth = $(this).next('.slider_fs_container').find('img:first').width();
		currentFsScrollOffset -= scrollFsWidth;
		$('.slider-fs-next').attr('disabled', false);
		if(currentFsScrollOffset <= 0) {
			currentFsScrollOffset = 0;
			$('.slider-fs-prev').attr('disabled', true);
		}
		scrollFsceenTo(currentFsScrollOffset, 0);
	});

	$('.container_gallery').find('img').click(function() {
		if ($(document).width() >= 960){
	        $('#container_content').addClass('blured');
	        $('#container_bigslider').css('display', 'flex');
    	};
	});

// FORM
	$(".quiz_base").submit (function () {
		var name = $("input[name='name']").val();
		var tell = $("input[name='tell']").val();
		var ans01 = $("input[name='ans01']:checked").val();
		var ans02 = $("input[name='ans02']:checked").val();
		var ans03 = $("input[name='ans03']:checked").val();
		var ans04 = $("input[name='ans04']:checked").val();
		var utm = $("input[name='utm_source']").val();

		$.ajax({
		  type: "POST",
		  url: "/scripts/sendmail.php",
		  data: {name: name, tell: tell, ans01: ans01, ans02: ans02, ans03: ans03, ans04: ans04, utm},
		  success: function(data) {
		    if(data) {
				$('.page_form').find('button').attr('disabled', true);
				$('.tablesample').addClass('activelink');
		    }
		    else {
		      alert("Не удалось отправить данные, попробуйте еще раз");
		    }

		  }

		});
		return false;
	});

});

function scrollTo(offset) {
	$('.slider_prev_container').animate({scrollLeft:offset}, 200); 
	return false; 
}

function scrollFsceenTo(offset) {
	$('.slider_fs_container').animate({scrollLeft:offset}, 200); 
	return false; 
}